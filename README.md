**How to use:**
- Run run_benchmark.sh, with the following parameters: 
    - path to your MyStack src directory
    - path to the input file containing your scenario definitions
    - [optional] path to a .csv file for exporting the measured data 
- Your MyStack class must include a public push(char) and a public pop() method
- Your MyStack class may not break when popping from an empty MyStack (EmptyStackExceptions are handled by MyStackBenchmark)

**Modes:**
- sequential:  [sampleSize] elements are pushed onto the stack and then popped.
- alternating: A scenario of [sampleSize] operations is created randomly with a ratio of 3/1 for push/pop operations and filled up with pop operations until parity is reached.

Sample size should be between 1 and 10000000

JITtoggle allows you to run the scenario with or without using the Just-In-Time compiler:
- on: Real-world scenario, as it is the default for any JVM
- off: For investigating the impact of the JIT compiler on the Stack implementations. Also yields more similar performance ratios across different platforms.

**INPUT file**:
- One scenario per line.
- Format: repeats mode JITtoggle sampleSize
- E.g.: 1000 sequential on 100000
    --> Performs 1000 repeats of pushing 100000 objects and then popping them, with JIT compiler turned on
- Unix line breaks only!

Issues:
- File must end with a line break or else last scenario will be ignored

**OUTPUT**

Console/Log output only contains the average measured value and performance ratio over all repeats:
```
SEQUENTIAL, JIT turned  ON, 100000 samples (average over 1000 repeats):
   Push:  0,586ms MyStack, 1,859ms Java Stack; Performance quotient: 4,55
   Pop:   0,279ms MyStack, 1,668ms Java Stack; Performance quotient: 6,14
   Total scenario runtime: 4,4s
ALTERNATING, JIT turned OFF, 100000 samples (average over 1000 repeats):
         13,132ms MyStack, 19,772ms Java Stack; Performance quotient: 1,51
   Total scenario runtime: 43,8s
```

CSV output includes the measured values from each run (may result in quite large files).

Example input and output files can be found in the "examples" folder