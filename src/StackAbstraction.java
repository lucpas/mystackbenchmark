import java.util.EmptyStackException;
import java.util.Stack;

enum StackTypes { JAVASTACK, MYSTACK }

class StackAbstraction {
    private final Stack javaStack;
    private final MyStack myStack;
    private final StackTypes typeUsed;

    StackAbstraction(StackTypes type){
        switch (type) {
            case MYSTACK:
                myStack = new MyStack();
                javaStack = null;
                typeUsed = StackTypes.MYSTACK;
                break;
            case JAVASTACK:
                myStack = null;
                javaStack = new Stack();
                typeUsed = StackTypes.JAVASTACK;
                break;
            default:
                myStack = null;
                javaStack = null;
                typeUsed = null;
        }
    }

    @SuppressWarnings("unchecked")
    void push(char c){
        switch (typeUsed) {
            case MYSTACK:
                myStack.push(c);
                break;
            case JAVASTACK:
                javaStack.push(c);
                break;
        }
    }

    @SuppressWarnings("UnusedReturnValue")
    char pop() throws EmptyStackException {
        Object c = null;

        switch (typeUsed) {
            case MYSTACK:
                c = myStack.pop();
            break;
            case JAVASTACK:
                c = javaStack.pop();
            break;
        }

        return (char)c;
    }
}
