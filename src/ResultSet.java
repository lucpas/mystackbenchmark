import java.util.EnumMap;
import java.util.Set;

enum ResultType {
    MYSTACK_tPUSH, MYSTACK_tPOP, MYSTACK_tTOTAL,
    JAVASTACK_tPUSH, JAVASTACK_tPOP, JAVASTACK_tTOTAL,
    PERFORMANCE_PUSH, PERFORMANCE_POP, PERFORMANCE_TOTAL }

class ResultSet {
    private static final double EMPTYFIELD_DEFAULT = -1.0;
    private final EnumMap<ResultType, Double> data;

    ResultSet() {
        data = new EnumMap<ResultType, Double>(ResultType.class);
    }

    void put(ResultType resType, double value) {
        data.put(resType, value);
    }

    double getValue(ResultType resType) {
        return data.getOrDefault(resType, EMPTYFIELD_DEFAULT);
    }

    private Set<ResultType> getResultTypes() {
        return data.keySet();
    }

    void calcQuotient(ResultType dividend, ResultType divisor, ResultType destination) {
        double resultValue = data.get(dividend) / data.get(divisor);
        data.put(destination, resultValue);
    }

    String getHeader_CSV() {
        StringBuilder output = new StringBuilder();

        for (ResultType type : ResultType.values()) {
            output.append(type.toString());
            output.append(';');
        }

        output.deleteCharAt(output.length() - 1);
        return output.toString();
    }

    String getData_CSV() {
        StringBuilder output = new StringBuilder();

        for (ResultType type : ResultType.values()) {
            double value = getValue(type);
            if(value != EMPTYFIELD_DEFAULT)
                output.append(value);
            output.append(';');
        }

        output.deleteCharAt(output.length() - 1);
        return output.toString();
    }

    static ResultSet meltIntoAverage(ResultSet[] results) {
        ResultSet meltedSet = new ResultSet();

        for (ResultType type : results[0].getResultTypes()){
            for (ResultSet currentSet : results) {
                double value = meltedSet.getValue(type) + currentSet.getValue(type);
                meltedSet.put(type, value);
            }

            double avg = meltedSet.getValue(type) / results.length;
            meltedSet.put(type, avg);
        }

        return meltedSet;
    }
}