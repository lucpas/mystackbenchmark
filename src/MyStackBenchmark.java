
class MyStackBenchmark {


    public static void main(String[] args) {
        TestConfiguration config = IOHelper.parseCLIParameters(args);
        ResultSet[] results = new ResultSet[config.getRepeats()];
        long totalTime = System.currentTimeMillis();

        //Run the scenario
        for (int i=0;i<results.length;i++) {
            results[i] = StackScenarios.run(config);
        }

        //Export raw data as .csv, if specified by starting param
        if (config.hasFileExportEnabled()) {
            IOHelper.exportCSV(config, results);
        }

        //Print average values and total runtime to console
        ResultSet average = ResultSet.meltIntoAverage(results);
        IOHelper.printToConsole(config, average);
        totalTime = System.currentTimeMillis() - totalTime;
        System.out.printf("   Total scenario runtime: %.1fs%n", totalTime/1000.0);
    }
}
