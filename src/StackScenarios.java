import java.util.EmptyStackException;
import java.util.Random;

enum Operation { PUSH, POP, DRYRUN }

class StackScenarios {

    static ResultSet run (TestConfiguration config) {
        final StackAbstraction testStack = new StackAbstraction(StackTypes.MYSTACK);
        final StackAbstraction refStack = new StackAbstraction(StackTypes.JAVASTACK);

        ResultSet results = new ResultSet();

        switch (config.getMode()) {
            case SEQUENTIAL:
                double pushTime = runSequential(config.getSampleSize(), Operation.PUSH, testStack);
                double popTime = runSequential(config.getSampleSize(), Operation.POP, testStack);
                results.put(ResultType.MYSTACK_tPUSH, pushTime);
                results.put(ResultType.MYSTACK_tPOP, popTime);
                results.put(ResultType.MYSTACK_tTOTAL, pushTime + popTime);

                pushTime = runSequential(config.getSampleSize(), Operation.PUSH, refStack);
                popTime = runSequential(config.getSampleSize(), Operation.POP, refStack);
                results.put(ResultType.JAVASTACK_tPUSH, pushTime);
                results.put(ResultType.JAVASTACK_tPOP, popTime);
                results.put(ResultType.JAVASTACK_tTOTAL, pushTime + popTime);

                results.calcQuotient(ResultType.JAVASTACK_tPUSH, ResultType.MYSTACK_tPUSH, ResultType.PERFORMANCE_PUSH);
                results.calcQuotient(ResultType.JAVASTACK_tPOP, ResultType.MYSTACK_tPOP, ResultType.PERFORMANCE_POP);
                results.calcQuotient(ResultType.JAVASTACK_tTOTAL, ResultType.MYSTACK_tTOTAL, ResultType.PERFORMANCE_TOTAL);
                break;
            case ALTERNATING:
                Operation[] sc = generateAlternatingScenario(config.getSampleSize());

                double totalTime = runAlternating(config.getSampleSize(), testStack, sc);
                results.put(ResultType.MYSTACK_tTOTAL, totalTime);

                totalTime = runAlternating(config.getSampleSize(), refStack, sc);
                results.put(ResultType.JAVASTACK_tTOTAL, totalTime);

                results.calcQuotient(ResultType.JAVASTACK_tTOTAL, ResultType.MYSTACK_tTOTAL, ResultType.PERFORMANCE_TOTAL);
                break;
            default:
                System.out.println("Error while selecting test mode!");
                System.exit(1);
        }

        return results;
    }

    private static double runAlternating(int sampleSize, StackAbstraction stack, Operation[] scenario) {
        long startTime = System.nanoTime();
        for (Operation op : scenario) {
            switch (op) {
                case PUSH:
                    stack.push('c');
                    break;
                case POP:
                    try {
                        stack.pop();
                    } catch (EmptyStackException ignore) {}
                    break;
            }
        }
        return convertNanosToMilliseconds(System.nanoTime() - startTime);
    }

    private static Operation[] generateAlternatingScenario(int sampleSize){
        Random r = new Random();
        Operation[] scenario = new Operation[sampleSize];

        //Push+pop are randomly distributed in the specified ratio in the first section of the scenario
        int ratioPushToPop = 3;
        int randomSamples = sampleSize - (sampleSize*(ratioPushToPop-1))/(2*ratioPushToPop);
        for (int i=0; i<randomSamples; i++) {
            scenario[i] = r.nextInt(ratioPushToPop + 2) == 1 ? Operation.POP : Operation.PUSH;
        }
        //2nd section is filled with pops to approximately even out the push/pop ratio in total
        for (int i=randomSamples; i<scenario.length; i++) {
            scenario[i] = Operation.POP;
        }
        return scenario;
    }

    private static double runSequential(int sampleSize, Operation operation, StackAbstraction stack) {
        long startTime;
        switch(operation) {
            case PUSH:
                startTime = System.nanoTime();
                for(int i=0; i<sampleSize; i++)
                    stack.push('a');
                break;
            case POP:
                startTime = System.nanoTime();
                for(int i=0; i<sampleSize; i++)
                    stack.pop();
                break;
            default:
                startTime = System.nanoTime();
                //noinspection LoopStatementThatDoesntLoop
                for(int i=0; i<sampleSize; //noinspection UnusedAssignment
                    i++)
                    //nothing here
                break;
        }
        return convertNanosToMilliseconds(System.nanoTime() - startTime);
    }

    private static double convertNanosToMilliseconds(long milli) {
        return milli/1000000.0;
    }
}