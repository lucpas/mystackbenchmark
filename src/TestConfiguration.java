import java.nio.file.Path;

enum Mode { SEQUENTIAL, ALTERNATING }
enum JITmode { ON, OFF }

class TestConfiguration {
    static final int DEFAULT_REPEATS = 1;
    static final int DEFAULT_SAMPLESIZE = 1000000;
    static final Mode DEFAULT_MODE = Mode.SEQUENTIAL;

    private static final String CSV_HEADER = "Mode;JIT;Samples";

    private final Mode mode;
    private final int sampleSize;
    private final int repeats;
    private JITmode jit;
    private Path exportPath;

    /*
        Constructor for setting mandatory and optional fields
     */
    TestConfiguration(Mode mode, JITmode jit, int sampleSize, int repeats, Path exportPath) {
        this.mode = mode;
        this.jit = jit;
        this.sampleSize = sampleSize;
        this.repeats = repeats;
        this.exportPath = exportPath;
    }

    /*
       Constructor for setting mandatory fields only
    */
    TestConfiguration(Mode mode, int sampleSize, int repeats) {
        this.mode = mode;
        this.sampleSize = sampleSize;
        this.repeats = repeats;
        this.jit = null;
        this.exportPath = null;
    }

    boolean hasFileExportEnabled() {
        return exportPath != null;
    }

    void setJit(JITmode jit) {
        this.jit = jit;
    }

    void setExportPath(Path exportPath) {
        this.exportPath = exportPath;
    }

    Mode getMode() {
        return mode;
    }

    int getSampleSize() {
        return sampleSize;
    }

    Path getExportPath() {
        return exportPath;
    }

    int getRepeats() {
        return repeats;
    }

    String getHeader_CSV() {
        return CSV_HEADER;
    }

    String getData_CSV() {
        return String.format("%s;%s;%d", mode.toString(), jit.toString(), sampleSize);
    }

    static TestConfiguration getDefaultConfig() {
        return new TestConfiguration(DEFAULT_MODE, JITmode.ON, DEFAULT_SAMPLESIZE, DEFAULT_REPEATS, null);
    }
}