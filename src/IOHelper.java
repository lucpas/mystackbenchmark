import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.*;

class IOHelper {

    static TestConfiguration parseCLIParameters(String[] args) {
        //Create basic config from obligatory params
        TestConfiguration config = parseObligatoryParams(args);

        //Check whether JIT compiler is turned on or off
        if (System.getProperty("java.vm.info").equals("interpreted mode"))
            config.setJit(JITmode.OFF);
        else
            config.setJit(JITmode.ON);

        //Modify config with optional parameters
        config = parseOptionalParams(args, config);

        return config;
    }

    /*
        Parse optional parameters (export file path)
        - if one or more are missing: ignore
        - if one or more are invalid: set it to null
    */
    private static TestConfiguration parseOptionalParams(String[] args, TestConfiguration config){
        try {
            config.setExportPath(Paths.get(args[3]));
        } catch (InvalidPathException | NullPointerException e) {
            System.out.println("Invalid file path parameter! No export will be performed.");
        } catch (ArrayIndexOutOfBoundsException e) {
            //File export is optional
        }

        return config;
    }

    /*
        Parse obligatory parameters (mode, sample size, repeats)
        - if one or more are missing: use the default configuration
        - if one or more are invalid: abort run
    */
    private static TestConfiguration parseObligatoryParams(String[] args){
        Mode mode = null;
        int repeats = 0;
        int sampleSize = 0;

        try {
            repeats = Integer.valueOf(args[0]);
            sampleSize = Integer.valueOf(args[1]);
            mode = Mode.valueOf(args[2].toUpperCase());
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.printf("Missing parameters! Loading default config: %d samples, %s, %d repeats",
                    TestConfiguration.DEFAULT_SAMPLESIZE,
                    TestConfiguration.DEFAULT_MODE.toString(),
                    TestConfiguration.DEFAULT_REPEATS);
            return TestConfiguration.getDefaultConfig();
        } catch (IllegalArgumentException e) {
            System.out.printf("One or more invalid parameters present: %s %s %s %n", args[0], args[1], args[2]);
            System.exit(1);
        }

        return new TestConfiguration(mode, sampleSize, repeats);
    }

    /*
        Print a result set and its configuration to console
     */
    static void printToConsole(TestConfiguration config, ResultSet results) {
        final String FORMAT_CLI_ALT =
                "         %6.3fms MyStack, %5.3fms Java Stack; Performance quotient: %.2f%n";
        final String FORMAT_CLI_SEQ =
                "   Push: %6.3fms MyStack, %5.3fms Java Stack; Performance quotient: %.2f%n" +
                "   Pop:  %6.3fms MyStack, %5.3fms Java Stack; Performance quotient: %.2f%n";

        //Each mode has their own format for console printing
        switch (config.getMode()) {
            case SEQUENTIAL:
                    System.out.printf(FORMAT_CLI_SEQ,
                            results.getValue(ResultType.MYSTACK_tPUSH),
                            results.getValue(ResultType.JAVASTACK_tPUSH),
                            results.getValue(ResultType.PERFORMANCE_PUSH),
                            results.getValue(ResultType.MYSTACK_tPOP),
                            results.getValue(ResultType.JAVASTACK_tPOP),
                            results.getValue(ResultType.PERFORMANCE_POP));
                break;
            case ALTERNATING:
                    System.out.printf(FORMAT_CLI_ALT,
                            results.getValue(ResultType.MYSTACK_tTOTAL),
                            results.getValue(ResultType.JAVASTACK_tTOTAL),
                            results.getValue(ResultType.PERFORMANCE_TOTAL));
                break;
        }
    }

    /*
        Export multiple result sets and the configuration as a .csv file
     */
    static void exportCSV(TestConfiguration config, ResultSet[] results) {
        String header = config.getHeader_CSV() + ";" + results[0].getHeader_CSV();
        String configText = config.getData_CSV() + ";";

        Path outputDest = config.getExportPath();

        try {
            //Create directories along the file path, if necessary
            if (outputDest.getParent() != null)
                Files.createDirectories(outputDest.getParent());

            BufferedWriter fw;
            //If file exists, append to it; if not, create a new one and write the .csv header
            if (Files.exists(outputDest)) {
                fw = Files.newBufferedWriter(outputDest, StandardOpenOption.APPEND);
            } else {
                fw = Files.newBufferedWriter(outputDest, StandardOpenOption.CREATE_NEW);
                fw.write(header);
                fw.newLine();
            }
            //Write each result set as one csv line
            for (ResultSet res : results) {
                String data = configText + res.getData_CSV();
                fw.write(data);
                fw.newLine();
            }
            fw.close();
        } catch (IOException e) {
            System.err.println("Error: Could not write to file " + outputDest.toAbsolutePath());
            System.exit(-3);
        }
    }
}
