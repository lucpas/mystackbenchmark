#!/bin/bash

BASEDIR=$1
TESTDATA=$2
EXPORTFILE=$3
LOGFILE="log.txt"

cd .

#If a logfile is already present either replace or append to it
if [[ -f $LOGFILE ]]; then
    read -p "Delete existing log? [y/n] " delete
    if [[ $delete == [yY] ]]; then
        rm "$LOGFILE" && echo "Removing previous log..." | tee -a $LOGFILE
    fi
fi

#Check if there is an input file and abort, if not
if [ ! -f $TESTDATA ]; then
  echo "Input file not found" | tee -a $LOGFILE
  exit 1
fi

echo "Expecting base sources in directory $BASEDIR" | tee -a $LOGFILE

#Try building jar and abort if build failed
ant package -S -Dsrc_bmrk.dir $BASEDIR | tee -a $LOGFILE
if [ $? -ne 0 ]; then
  echo "BUILD ERROR" | tee -a $LOGFILE
  exit 2
else
  echo "BUILD SUCCESSFUL" | tee -a $LOGFILE
  echo "----------------" | tee -a $LOGFILE
fi

scCount_total=$(wc -l < "$TESTDATA")
scCount_current=0

echo "Processing $scCount_total scenarios:"
while read repeats mode JITtoggle samples; do
  #Increment counter
  ((scCount_current++))

  echo "  $scCount_current of $scCount_total: $mode, JIT $JITtoggle, $samples samples, $repeats repeats..."
  
  #Check whether all parameter are given
  if [ -z "$samples" ]; then
    echo "... Missing vital parameters, discarding scenario!"
	continue
  fi
  
  #Make mode and JITtoggle uppercase
  #mode=${mode^^}
  #JITtoggle=${JITtoggle^^}
  mode=$(echo $mode | tr a-z A-Z)
  JITtoggle=$(echo $JITtoggle | tr a-z A-Z)  
  
  #Start log entry
  printf "%s, JIT turned %3s, %d samples (average over %d repeats):\n" $mode $JITtoggle $samples $repeats >> $LOGFILE
 
  #Catch invalid JIT options
  if [ ! $JITtoggle = "ON" ] && [ ! $JITtoggle = "OFF" ]; then
	echo "...Invalid JITtoggle "\""$JITtoggle"\"", discarding scenario!" | tee -a $LOGFILE
	continue
  fi
 
  #Run scenario and write output to log
  if [ $JITtoggle = "ON" ]; then
	java -jar MyStackBenchmark.jar ${repeats} ${samples} ${mode} ${EXPORTFILE} >> $LOGFILE
  else #[ $JITtoggle = "OFF" ]
    java -Djava.compiler=NONE -jar MyStackBenchmark.jar ${repeats} ${samples} ${mode} ${EXPORTFILE}  >> $LOGFILE
	
  if [ $? -ne 0 ]; then
    echo "...Execution failed!"
#  else
#    echo "...Execution successful!"
  fi
fi
done < $TESTDATA

echo "----------------" | tee -a $LOGFILE
echo "Very success! Much wow!"